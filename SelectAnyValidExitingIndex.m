function exitingIndex = SelectAnyValidExitingIndex(...
    currentBasis, ...
    currentBasisSolutionAtBasisIndices, ...
    w, ...
    tolerance ...
)
    % Selects a valid index to exit the basis in the Ratio Test stage
    % without using a specific rule.
    % When performing the Simplex algorithm and reaching the Ratio Test stage
    % in an arbitrary iteration and the algorithm does not terminate
    % due to the LP not being bounded,
    % then this function can be used to select an index
    % which shall exit the basis without using a specific rule.
    % In order to account for rounding errors the provided tolerance is accepted
    % in the following sense.
    % From all indices i between 1 and m the function selects one for which
    % - the i-th component of w, called w_i, is larger than the tolerance and
    % - the i-th basis component of the basis solution, called x_B_i,
    %   divided by w_i is equal to gamma, i.e.
    %       x_B_i / w_i = gamma.
    %   (Using the index with which gamma is defined allows to neglect
    %   rounding errors here.)
    % The exiting index is the corresponding basis index
    % (i.e. the element of currentBasis at this index)
    % This ensures that even with rounding erros within the tolerance,
    % only positive values of w are considered.
    %
    % Auxiliary variables: n - the number of variables
    %                          in the linear optimization problem
    %                      m - the number of side constraints
    %                          except non-negativity
    %
    % Input: currentBasis - m dimensional row vector containing the basis
    %        currentBasisSolutionAtBasisIndices - m dimensional column vector
    %                                             whose elements are the basis
    %                                             components of the
    %                                             basis solution
    %        w - m dimensional row vector containing the "w-vector"
    %            defined in the FTRAN stage.
    %        tolerance - a non-negative real number
    %                    which defines the applied tolerance
    % Output: exitingIndex - index (natural number) exiting the basis
    %
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    indicesWhereWIsPositive = find(w > tolerance);
    [~, minQuotientsIndex] = min(...
        currentBasisSolutionAtBasisIndices(indicesWhereWIsPositive) ./ w(indicesWhereWIsPositive) ...
    );
    exitingIndex = currentBasis(indicesWhereWIsPositive(minQuotientsIndex));
end
