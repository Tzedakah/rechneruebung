function [xOpt, B, message, iter, costFctValue] = SimplexBland(A, b, c, BInit, xB, displayMode)
    % This function performs the simplex algorithm to find the minimum point and value
    % of a linear optimization problem or to find out that the problem is unbounded.
    % The cost function maps any feasible point x to the scalar product of the input parameter c and x.
    % A point x is feasible if and only if it is non-negative and satisfies the constraint Ax = b.
    % This function uses the Bland rule for the simplex algorithm.
    % After a million iterations the algorithm is terminated and an appropriate message is returned
    % to avoid infinitive loops.
    %
    % Technical details: In order to account for rounding errors a tolerance is defined.
    % The tolerance is amongst others used to soften the termination criteria
    % (The higher the tolerance the faster terminates the algorithm).
    % This means we use the following termination criteria:
    % - Pricing: If every component of the reduced costs is
    %            as least as big as the negative tolerance,
    %            then the optimal basis is assumed to be found.
    % - Ratio-Test: If every component of w is at most as big as the tolerance,
    %               then the LP is assumed to be unbounded.
    % Similarily other comparisons are assumed to be fulfilled,
    % if they are fulfilled +/- the tolerance.
    %
    % Auxiliary variables: n - the number of variables in the linear optimization problem
    %                      m - the number of constraints for the definition of feasible points except non-negativity
    %
    % Input: A - mxn matrix of reals with full rank and n >= m.
    %            This is the constraint matrix of the linear optimization problem.
    %        b - m dimensional column vector of reals.
    %            This vector contains the right sides of the constraints demanding equality (Ax = b).
    %            A point must satisfy Ax = b to be feasible in the linear optimization problem
    %        c - n dimensional column vector of reals.
    %            This vector defines the cost function of linear optimization problem,
    %            which maps every feasible point x to the scalar product of c and x.
    %        BInit - m dimensional row vector of natural numbers
    %                which forms a primal feasible basis of the linear optimization problem.
    %                This is the basis with which the Simplex Algorithm will start.
    %        xB - m dimensional column vector of reals which contains those components
    %             of the basis solution belonging to BInit which are selected by the basis BInit
    %        displayMode - (optional with default: false). A boolean.
    %                      If and only if set to true the following is displayed
    %                      at the end of every iteration:
    %                      - The iteration number
    %                      - The updated basis
    % Output: xopt - n dimensional column vector of reals.
    %                This is a minimal point and basis solution of the linear optimization problem.
    %                If no such point exists because the linear optimization problem is unbounded,
    %                then this is set to NaN.
    %                If the algorithm does not terminate in a million iterations, it is terminated
    %                and this is set to NaN.
    %         B - m row dimensional vector of natural numbers.
    %             This is an optimal and primal feasible basis of the linear optimization problem
    %             and xOpt is the corresponding basis solution.
    %             If no such basis exists because the linear optimization problem is unbounded,
    %             then this is set to NaN.
    %             If the algorithm does not terminate in a million iterations, it is terminated
    %             and this is set to NaN.
    %         message - a string containing a message with the information whether a minimum was found
    %                   for the linear optimization problem, the problem is unbounded
    %                   or the algorithm was force quit after a million iterations.
    %         iter - natural number with the number of iterations of the simplex algorithm
    %                which were performed by this function.
    %         costFctValue - (real) minimum value of the linear optimization problem,
    %                        ie. the value of the cost function at xOpt.
    %                        If no such point exists because the linear optimization problem is unbounded,
    %                        then this is set to NaN.
    %                        If the algorithm does not terminate in a million iterations, it is terminated
    %                        and this is set to NaN.
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    % Set displayMode by default to false unless specified otherwise
    % by an argument
    if nargin < 6
        displayMode = false;
    end

    [xOpt, B, message, iter, costFctValue] = GenericSimplex(A, b, c, BInit, xB, 'Bland', displayMode);
end
