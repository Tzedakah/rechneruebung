function [xOpt, B, message, iter, costFctValue] = GenericSimplex(A, b, c, BInit, xB, rule, displayMode)
    % This function performs the simplex algorithm to find the minimum point and value
    % of a linear optimization problem or to find out that the problem is unbounded.
    % The cost function maps any feasible point x to the scalar product of the input parameter c and x.
    % A point x is feasible if and only if it is non-negative and satisfies the constraint Ax = b.
    % This function uses the specified rule for the simplex algorithm.
    % The argument for rule must be either Dantzig or Bland!
    % After a million iterations the algorithm is terminated and an appropriate message is returned
    % to avoid infinitive loops.
    %
    % Technical details: In order to account for rounding errors a tolerance is defined.
    % The tolerance is amongst others used to soften the termination criteria
    % (The higher the tolerance the faster terminates the algorithm).
    % This means we use the following termination criteria:
    % - Pricing: If every component of the reduced costs is
    %            as least as big as the negative tolerance,
    %            then the optimal basis is assumed to be found.
    % - Ratio-Test: If every component of w is at most as big as the tolerance,
    %               then the LP is assumed to be unbounded.
    % Similarily other comparisons are assumed to be fulfilled,
    % if they are fulfilled +/- the tolerance.
    %
    % Auxiliary variables: n - the number of variables in the linear optimization problem
    %                      m - the number of constraints for the definition of feasible points except non-negativity
    %
    % Input: A - mxn matrix of reals with full rank and n >= m.
    %            This is the constraint matrix of the linear optimization problem.
    %        b - m dimensional column vector of reals.
    %            This vector contains the right sides of the constraints demanding equality (Ax = b).
    %            A point must satisfy Ax = b to be feasible in the linear optimization problem
    %        c - n dimensional column vector of reals.
    %            This vector defines the cost function of linear optimization problem,
    %            which maps every feasible point x to the scalar product of c and x.
    %        BInit - m dimensional row vector of natural numbers
    %                which forms a primal feasible basis of the linear optimization problem.
    %                This is the basis with which the Simplex Algorithm will start.
    %        xB - m dimensional column vector of reals which contains those components
    %             of the basis solution belonging to BInit which are selected by the basis BInit
    %        rule - string containing the rule which will be used for selecting
    %               entering and exiting index in the Pricing and the Ratio Test.
    %               Must be either 'Dantzig' or 'Bland'.
    %        displayMode - (optional with default: false). A boolean.
    %                      If and only if set to true the following is displayed
    %                      at the end of every iteration:
    %                      - The iteration number
    %                      - The updated basis
    % Output: xopt - n dimensional column vector of reals.
    %                This is a minimal point and basis solution of the linear optimization problem.
    %                If no such point exists because the linear optimization problem is unbounded,
    %                then this is set to NaN.
    %                If the algorithm does not terminate in a million iterations, it is terminated
    %                and this is set to NaN.
    %         B - m row dimensional vector of natural numbers.
    %             This is an optimal and primal feasible basis of the linear optimization problem
    %             and xOpt is the corresponding basis solution.
    %             If no such basis exists because the linear optimization problem is unbounded,
    %             then this is set to NaN.
    %             If the algorithm does not terminate in a million iterations, it is terminated
    %             and this is set to NaN.
    %         message - a string containing a message with the information whether a minimum was found
    %                   for the linear optimization problem, the problem is unbounded
    %                   or the algorithm was force quit after a million iterations.
    %         iter - natural number with the number of iterations of the simplex algorithm
    %                which were performed by this function.
    %         costFctValue - (real) minimum value of the linear optimization problem,
    %                        ie. the value of the cost function at xOpt.
    %                        If no such point exists because the linear optimization problem is unbounded,
    %                        then this is set to NaN.
    %                        If the algorithm does not terminate in a million iterations, it is terminated
    %                        and this is set to NaN.
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    % Set displayMode by default to false unless specified otherwise
    % by an argument
    if nargin < 7
        displayMode = false;
    end

    % Tolerance to account for computational accuracy in comparisons
    tolerance = 1e-6;

    [~, n] = size(A);

    checkIfInputIsValid(A, b, c, BInit, xB, tolerance);

    currentBasis = BInit;
    nonBasisIndices = setdiff(1:n, currentBasis);

    currentBasisSolutionAtBasisIndices = xB;

    iter = 0;
    % At most a million iterations
    while iter < 1e6
        iter = iter + 1;

        basisMatrix = A(:, currentBasis);

        % BTRAN
        y = basisMatrix' \ c(currentBasis);

        % Pricing
        reducedCostsNonBasis = c(nonBasisIndices) - A(:, nonBasisIndices)' * y;

        if reducedCostsNonBasis >= -tolerance
            xOpt = zeros(n, 1);
            xOpt(currentBasis) = currentBasisSolutionAtBasisIndices;
            B = currentBasis;
            message = "The minimal point and value were successfully found with the simplex algorithm!";
            costFctValue = dot(c, xOpt);
            return
        end

        switch rule
            case 'Dantzig'
                enteringIndex = SelectEnteringIndexUsingDantzig(...
                    reducedCostsNonBasis,...
                    nonBasisIndices ...
                );
            case 'Bland'
                enteringIndex = SelectEnteringIndexUsingBland( ...
                    reducedCostsNonBasis, ...
                    nonBasisIndices, ...
                    tolerance ...
                );
            otherwise
                error([...
                    'The argument for the parameter rule ', ...
                    'must be Dantzig or Bland!' ...
                ]);
        end

        % FTRAN
        w = basisMatrix \ A(:, enteringIndex);

        % Ratio-Test
        if w <= tolerance
            xOpt = NaN;
            B = NaN;
            message = "No minimum exists because the linear problem is unbounded!";
            costFctValue = NaN;
            return
        end

        gamma = min(currentBasisSolutionAtBasisIndices(w > tolerance) ./ w(w > tolerance));
        switch rule
            % Dantzig has no special selection rule for the Ratio Test.
            % This is why any valid exiting index is selected
            case 'Dantzig'
                exitingIndex = SelectAnyValidExitingIndex(...
                    currentBasis, ...
                    currentBasisSolutionAtBasisIndices, ...
                    w, ...
                    tolerance ...
                );
            case 'Bland'
                exitingIndex = SelectExitingIndexUsingBland(...
                    currentBasis, ...
                    currentBasisSolutionAtBasisIndices, ...
                    w, ...
                    tolerance ...
                );
            otherwise
                error([...
                    'The argument for the parameter rule ', ...
                    'must be Dantzig or Bland!' ...
                ]);
        end

        % Update
        currentBasisSolutionAtBasisIndices = currentBasisSolutionAtBasisIndices - gamma * w;
        currentBasis(currentBasis==exitingIndex) = enteringIndex;
        nonBasisIndices = setdiff(1:n, currentBasis);
        currentBasisSolutionAtBasisIndices(find(currentBasis == enteringIndex)) = gamma;

        % Display iteration number and updated basis if display mode is active
        if (displayMode)
            disp(['Iteration: ', num2str(iter)]);
            fprintf('%s [%s]\n', 'Basis: ', num2str(currentBasis));
        end
    end

    xOpt = NaN;
    B = NaN;
    message = "The algorithm was canceled after a million iterations.";
    costFctValue = NaN;
end

function checkIfInputIsValid(A, b, c, BInit, xB, tolerance)
    % Checks if the specified arguments meet the requirements
    % placed on the identically named parameters
    % of the function GenericSimplex.
    % To be precise this function checks the following requirements.
    % - If the input parameters have the correct dimensions.
    % - If A has full rank.
    % - If BInit is a primal feasible basis
    %   in the LP in standard form given by A,b and c
    % - If xB defines a basis solution for the basis BInit
    %   in the considered LP
    % If any of these requirements is not fulfilled,
    % then an error with an appropriate message will be raised.
    % In order to account for rounding errors the provided tolerance is accepted
    % when checking the requirements above
    % (for details see in the Technical details)
    %
    % Technical details: Consider the basis matrix A_BInit consisting of
    % the columns of A whose indices are in BInit (in that order).
    % Part of the requirements is checking that it holds A_BInit * xB = b.
    % Accepting the specified tolerance means to only check that
    %    |A_BInit * xB - b| < tolerance
    % holds.
    %
    % Auxiliary variables: n - the number of rows in A
    %                      m - the number of columns in A
    %
    % Input: A - a matrix for which is checked
    %            whether it has more columns than rows and full rank.
    %        b - a vector for which is checked
    %            whether it is a m-dimensional column vector
    %        c - a vector for which is checked
    %            whether it is a n-dimensional column vector
    %        BInit - a vector for which is checked
    %            whether it is a m-dimensional row vector
    %            and a primal feasible basis
    %        xB - a vector for which is checked
    %            whether it is a m-dimensional column vector and
    %            fulfills A_B * xB = b,
    %            where A_B is the matrix consisting of the columns of A
    %            specified by BInit.
    %        tolerance - a non-negative real number
    %                    which defines the applied tolerance

    checkForCorrectDimensions(A, b, c, BInit, xB);
    checkFullRank(A);
    checkIfPrimalFeasibleBasisWithBasisSolution(BInit, xB, A, b, tolerance);
end

function hasFullRank = isFullRankMatrix(A)
    % Determines whether the specified matrix has full rank.
    %
    % Input: A - a matrix
    %
    % Output: hasFullRank - Logical 1 (true) if and only if A has full rank.
    %                       Logical 0 (false) otherwise.

    hasFullRank = rank(A) == min(size(A));
end

function checkFullRank(A)
    % Checks if the given matrix A has full rank.
    % If this is not the case,
    % an error with an appropriate message will be raised.
    %
    % Input: A - a matrix for which is checked it it has full rank

    if (~isFullRankMatrix(A))
        error('The argument A is expected to have full rank!');
    end
end

function checkIfBasisSolution(A, b, Basis, xB, tolerance)
    % Checks if a given point xB contains the basis components
    % of a basis solution for a LP in standard form
    % with the constraint Ax = b.
    % In other words, the function considers the basis matrix A_Basis
    % consisting of the columns of A whose indices are in Basis
    % (in the order given by Basis).
    % Then it checks that
    %     A_Basis * xB = b
    % holds (up to tolerance - for details see below).
    % If that is not the case an error is raised.
    %
    % Technical details: Accepting the specified tolerance means
    % that the function only check that
    %    |A_Basis * xB - b| < tolerance
    % holds.
    %
    % Input: - A: The coefficient matrix of the linear (constraint) system.
    %        - b: The right-hand side vector of the linear (constraint) system.
    %        - Basis: The basis for which is checked
    %                 if xB defines a corresponding basis solution
    %        - xB: The point to be checked for defining a basis solution.
    %        - tolerance - a non-negative real number
    %                      which defines the applied tolerance
    %
    % Throws an error if xB doesn't define a basis solution.

    % Extract the columns corresponding to the basis
    basisMatrix = A(:, Basis);

    if (abs(basisMatrix * xB - b) > tolerance)
        error_message = strcat(...
            'The given value for the parameter xB is invalid,', ...
            ' because it isn''t a basis solution', ...
            ' for the given basis BInit', ...
            ' in the context of the linear (constraint) system', ...
            ' given by A and b!' ...
        );
        error(error_message);
    end
end

function checkForCorrectDimensions(A, b, c, BInit, xB)
    % This function checks that the input arguments are vectors / matrices
    % with the expected dimensions.
    % For every input argument the expected dimension is the dimension
    % which the function GenericSimplex expects for the identically named
    % input parameter.
    % If an input argument has an unexpected dimension,
    % then an error with an appropriate message will be raised.
    %
    % Auxiliary variables: n - the number of rows in A
    %                      m - the number of columns in A
    %
    % Input: A - a matrix for which is checked
    %            whether it has more columns than rows
    %        b - a vector for which is checked
    %            whether it is a m-dimensional column vector
    %        c - a vector for which is checked
    %            whether it is a n-dimensional column vector
    %        BInit - a vector for which is checked
    %            whether it is a m-dimensional row vector
    %        xB - a vector for which is checked
    %            whether it is a m-dimensional column vector

    if (~ismatrix(A))
        error('The argument A is expected to be a m-by-n matrix');
    end

    [m, n] = size(A);

    if (m > n)
        error('The argument A is expected to have more columns than rows!');
    end

    checkIfColumnVectorWithRowLengthOfA(b, 'b', A);
    checkIfColumnVectorWithColumnLengthOfA(c, 'c', A);
    checkIfRowVectorWithRowLengthOfA(BInit, 'BInit', A);
    checkIfColumnVectorWithRowLengthOfA(xB, 'xB', A);
end

function checkIfColumnVectorWithRowLengthOfA(checkedVector, argumentName, A)
    % This function checks that checkedVector is a column vector
    % whose length is equal to the number of rows in A.
    % If this is not the case
    % an error with an appropriate message will be raised.
    %
    % Input: checkedVector: a vector for which is checked
    %                       whether it is column vector
    %                       whose length is equal to the number of rows in A.
    %        argumentName - name of the variable
    %                       passed as argument for checkedVector
    %        A - a matrix

    [m, ~] = size(A);
    hasExpectedSize = iscolumn(checkedVector) && (length(checkedVector) == m);
    if (~hasExpectedSize)
        message_template = strcat(...
            'The argument %s', ...
            ' is expected to be a column vector', ...
            ' whose length is equal to the numer of rows in the argument A!' ...
        );
        error(message_template, argumentName);
    end
end

function checkIfColumnVectorWithColumnLengthOfA(checkedVector, argumentName, A)
    % This function checks that checkedVector is a column vector
    % whose length is equal to the number of columns in A.
    % If this is not the case
    % an error with an appropriate message will be raised.
    %
    % Input: checkedVector: a vector for which is checked
    %                       whether it is column vector
    %                       whose length is equal to the number of columns in A.
    %        argumentName - name of the variable
    %                       passed as argument for checkedVector
    %        A - a matrix

    [~, n] = size(A);
    hasExpectedSize = iscolumn(checkedVector) && (length(checkedVector) == n);
    if (~hasExpectedSize)
        message_template = strcat(...
            'The argument %s ', ...
            ' is expected to be a column vector', ...
            ' whose length is equal to', ...
            ' the numer of columns in the argument A!' ...
        );
        error(message_template, argumentName);
    end
end

function checkIfRowVectorWithRowLengthOfA(checkedVector, argumentName, A)
    % This function checks that checkedVector is a row vector
    % whose length is equal to the number of rows in A.
    % If this is not the case
    % an error with an appropriate message will be raised.
    %
    % Input: checkedVector: a vector for which is checked
    %                       whether it is row vector
    %                       whose length is equal to the number of rows in A.
    %        argumentName - name of the variable
    %                       passed as argument for checkedVector
    %        A - a matrix

    [m, ~] = size(A);
    hasExpectedSize = isrow(checkedVector) && (length(checkedVector) == m);
    if (~hasExpectedSize)
        message_template = strcat(...
            'The argument %s', ...
            ' is expected to be a row vector', ...
            ' whose length is equal to the numer of rows in the argument A!' ...
        );
        error(message_template, argumentName);
    end
end

function checkIfPrimalFeasibleBasisWithBasisSolution(BInit, xB, A, b, tolerance)
    % Consider the linear optimization problem in standard form
    % whose side conditions are given by Ax=b (and x non-negative).
    % This function checks that BInit is a primal feasible basis
    % for that LP and that xB contains the basis components
    % of the corresponding basis solution.
    % This includes checking that
    % - BInit consists of pairwise distinct and non-zero integers
    % - BInit has no element larger than the number of columns in A
    % - The columns of A whose indeces are contained in BInit
    %   are linearily independent.
    % - BInit is not only a basis, but also a primal feasible one
    %   (i.e. the basis solution has no negative element)
    % - Consider the basis matrix A_BInit consisting of
    %   the columns of A whose indices are in BInit (in that order).
    %   Then it holds
    %       A_BInit * xB = b
    %   (up to tolerance - for details see below).
    % If any of the requirements above are not fulfilled,
    % an error with an appropriate message will be raised.
    % In order to account for rounding errors the provided tolerance is accepted
    % when checking the requirements above
    % (for details see in the Technical details)
    %
    % Technical details: The last check listed above, is to check that it holds
    %     A_BInit * xB = b.
    % Accepting the specified tolerance means to only check that
    %    |A_BInit * xB - b| < tolerance
    % holds.
    %
    % Auxiliary variables: m - the number of columns in A
    %
    % Input: Binit: m dimensional row vector for which is checked
    %               that it is a primal feasible basis
    %        xB: m dimensional column vector of reel numbers
    %            for which is checked that it is the basis representation
    %            of the basis solution.
    %        A: a matrix of reel numbers with more columns than rows
    %           and full rank which is the side condition matrix
    %        b: m dimensional column vector of reel numbers
    %           which is the right side of the side conditions
    %        tolerance - a non-negative real number
    %                    which defines the applied tolerance

    checkIfAllElementsAreColumnIndicesOfA(BInit, A);
    checkIfAllElementsAreUnique(BInit);

    basisMatrix = A(:, BInit);
    checkIfFullRank(basisMatrix);

    checkIfBasisSolution(A, b, BInit, xB, tolerance);

    % Form the checks above we know that BInit is a basis for the
    % LP in standard from given by the arguments A, b and c.
    % In the check above we confirmed that xB is this unique solution
    % of the lineaar equation system basisMatrix * basisSolution = b,
    % where basisSolution can be any reel vector with a suitable length.
    % To determine whether BInit is a primal feasible basis,
    % we need to determine whether the unique solution of this linear equation,
    % which is xB, is non-negative.
    if (any(xB < 0))
        error_message = strcat(...
            'The given value for the parameter BInit is invalid,', ...
            ' because BInit is a basis, but not a primal feasible basis', ...
            ' for the LP in standard form', ...
            ' given by the arguments A, b and c!', ...
            ' The basis solution defined by BInit', ...
            ' has at least one negative component!' ...
        );
        error(error_message);
    end

end

function checkIfAllElementsAreColumnIndicesOfA(BInit, A)
    % This is a auxiliary function for checking
    % that BInit is a primal feasible basis
    % for a linear optimization problem.
    % This function checks that BInit consists of valid indices
    % for the columns of the matrix A.
    % In other words, this functions checks that all elements of BInit
    % are non-zero integers that are at most as big as
    % the number of columns in A.
    % If this is not the case,
    % an error with an appropriate message will be raised.
    %
    % Input: Binit: m dimensional row vector for which is checked
    %               that it consists of valid indices for the columns of A.
    %        A: a matrix

    [~, n] = size(A);

    isBInitConsistingOfIndices = all(ismember(BInit, 1:n));
    if (~isBInitConsistingOfIndices)
        error_message = strcat(...
            'The given value for the parameter BInit is invalid,', ...
            ' because BInit is not a basis', ...
            ' for the LP in standard form', ...
            ' given by the arguments A, b and c!', ...
            ' BInit contains at least one element', ...
            ' that is not a valid index for a column of the matrix A!' ...
        );
        error(error_message);
    end
end

function checkIfAllElementsAreUnique(BInit)
    % This is a auxiliary function for checking
    % that BInit is a primal feasible basis
    % for a linear optimization problem.
    % This function checks that BInit consists of pairwise distinct elements.
    % If this is not the case,
    % an error with an appropriate message will be raised.
    %
    % Input: Binit: vector for which is checked that all its elements are unique

    hasOnlyUniqueElements = length(BInit) == length(unique(BInit));
    if (~hasOnlyUniqueElements)
        error_message = strcat(...
            'The given value for the parameter BInit is invalid,', ...
            ' because BInit is not a basis!', ...
            ' BInit contains at least one element multiple times!' ...
        );
        error(error_message);
    end
end

function checkIfFullRank(basisMatrix)
    % This is a auxiliary function for checking
    % that BInit is a primal feasible basis
    % for a linear optimization problem.
    % This function checks that the specified basisMatrix has full rank.
    % If this is not the case,
    % an error with an appropriate message will be raised.
    %
    % Input: basisMatrix: basis matrix of a LP

    if (~isFullRankMatrix(basisMatrix))
        error_message = strcat(...
            'The given value for the parameter BInit is invalid,', ...
            ' because BInit is not a basis', ...
            ' for the LP in standard form', ...
            ' given by the arguments A, b and c!', ...
            ' The columns of the matrix A', ...
            ' which are selected by BInit are not linearly independent!' ...
        );
        error(error_message);
    end
end
