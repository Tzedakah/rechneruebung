function exitingIndex = SelectExitingIndexUsingBland(...
    currentBasis, ...
    currentBasisSolutionAtBasisIndices, ...
    w, ...
    tolerance ...
)
    % Selects an index to exit the basis in the Ratio Test stage
    % according to the Bland rule.
    % When performing the Simplex algorithm and reaching the Ratio Test stage
    % in an arbitrary iteration and the algorithm does not terminate
    % due to the LP not being bounded,
    % then this function can be used to select the index
    % which shall exit the basis in accordance with the Bland rule.
    % In order to account for rounding errors the provided tolerance is accepted
    % in the following sense.
    % Of all indices i between 1 and m the function collects all those in a set,
    % for which
    % - the i-th component of w, called w_i, is larger than the tolerance and
    % - the i-th basis component of the basis solution, called x_B_i,
    %   divided by w_i is equal to gamma up to rounding errors, i.e.
    %       | x_B_i / w_i - gamma | < tolerance .
    % For each of these indices the corresponding basis index
    % (i.e. the element of currentBasis at this index)
    % is collected in a new set.
    % The exiting index is the smallest index in this new set.
    % This ensures that even with rounding erros within the tolerance,
    % only positive values of w are considered.
    %
    % Auxiliary variables: n - the number of variables
    %                          in the linear optimization problem
    %                      m - the number of side constraints
    %                          except non-negativity
    %
    % Input: currentBasis - m dimensional row vector containing the basis
    %        currentBasisSolutionAtBasisIndices - m dimensional column vector
    %                                             whose elements are the basis
    %                                             components of the
    %                                             basis solution
    %        w - m dimensional row vector containing the "w-vector"
    %            defined in the FTRAN stage.
    %        tolerance - a non-negative real number
    %                    which defines the applied tolerance
    % Output: exitingIndex - index (natural number) exiting the basis
    %                         according to the Dantzig rule
    %
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    indicesWhereWIsPositive = find( w > tolerance);
    quotients = currentBasisSolutionAtBasisIndices(indicesWhereWIsPositive) ./ w(indicesWhereWIsPositive);
    gamma = min(quotients);
    indicesWhereWIsPositveAndQuotientIsGamma = indicesWhereWIsPositive(...
        find(abs(quotients - gamma) < tolerance) ...
    );
    exitingIndex = min(currentBasis(indicesWhereWIsPositveAndQuotientIsGamma));
end