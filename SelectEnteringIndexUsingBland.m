function enteringIndex = SelectEnteringIndexUsingBland(reducedCostsNonBasis, nonBasisIndices, tolerance)
    % Selects an index to enter the basis in the Pricing stage
    % according to the Bland rule.
    % When performing the Simplex algorithm and reaching the Pricing stage
    % in an arbitrary iteration and the basis is not yet optimal,
    % then this function can be used to select the index
    % which shall enter the basis in accordance with the Bland rule.
    % In order to account for rounding errors the provided tolerance is accepted
    % in the following sense.
    % Of all non basis indices the function collects all those in a set,
    % for which the corresponding component of the reduced costs
    % is smaller than -tolerance.
    % The entering index is the smallest of these indices.
    % This ensures that even with rounding erros within the tolerance,
    % the reduced costs at the entering index are negative.
    %
    % Auxiliary variables: n - the number of variables
    %                          in the linear optimization problem
    %                      m - the number of side constraints
    %                          except non-negativity
    %
    % Input: reducedCostsNonBasis - (n-m) dimensional column vector
    %                               of the reduced costs
    %        nonBasisIndices - (n-m) dimensional row vector
    %                          with the indeces (natural numbers)
    %                          not contained in the basis
    %        tolerance - a non-negative real number
    %                    which defines the applied tolerance
    % Output: enteringIndex - index (natural number) entering the basis
    %                         according to the Dantzig rule
    %
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    indizesWhereReducedCostsIsNegative = nonBasisIndices(...
        find(reducedCostsNonBasis < -tolerance) ...
    );
    enteringIndex = min(indizesWhereReducedCostsIsNegative);
end