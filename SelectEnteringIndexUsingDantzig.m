function enteringIndex = SelectEnteringIndexUsingDantzig(reducedCostsNonBasis, nonBasisIndices)
    % Selects an index to enter the basis in the Pricing stage
    % according to the Dantzig rule.
    % When performing the Simplex algorithm and reaching the Pricing stage
    % in an arbitrary iteration and the basis is not yet optimal,
    % then this function can be used to select the index
    % which shall enter the basis in accordance with the Dantzig rule.
    %
    % Auxiliary variables: n - the number of variables
    %                          in the linear optimization problem
    %                      m - the number of side constraints
    %                          except non-negativity
    %
    % Input: reducedCostsNonBasis - (n-m) dimensional column vector
    %                               of the reduced costs
    %        nonBasisIndices - (n-m) dimensional row vector
    %                          with the indeces (natural numbers)
    %                          not contained in the basis
    % Output: enteringIndex - index (natural number) entering the basis
    %                         according to the Dantzig rule
    %
    % Authors: Aran Talaei, Samuel Graf
    % Date: 25.01.2024

    [~, enteringIndexRelativeToNonBasis] = min(reducedCostsNonBasis);
    enteringIndex = nonBasisIndices(enteringIndexRelativeToNonBasis);
end